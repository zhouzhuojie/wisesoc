********************
Introduction
********************

WiseSoc_ is a basic framework for building social network crawlers over the Amazon AWS cloud services. Generally, it integrated multiple online social network resources with their open APIs, such as Twitter, Facebook, Google+, etc. All their RESTful APIs are fully available through their OAuth interface. But almost all these APIs have rate limit which against data harvesters. WiseSoc leverages the power of Amazon AWS to build scalable sampling and crawling framework for online social networks.

.. _WiseSoc: https://bitbucket.org/zhouzhuojie/wisesoc/overview

Below is the architecture_ of WiseSoc.

.. _architecture: ./_static/Arch.jpg

********************
Tutorial
********************

********************
Resources
********************

* Twitter

  * Followers
  * Followings
  * Tweets

* Facebook

  * Friends
  * Posts

* Google+

  * Followers
  * Followings
  * Posts
  * Circles (Optional)

* Linkedin

********************
Components
********************

aws.py
===========

* SQS Services. SQS is used for creating crawling task queue. ::

   create_crawler_queue()
   write_crawler_queue()
   read_crawler_queue()
   delete_crawler_queue()
   clear_crawler_queue()

* SimpleDB Services. SimpleDB serves as the cache, and it store two types tables. ::

   SimpleDB Wrapper

* S3 Services. S3 storage serves as the cheap and flexible storage solution for those entries exceeds the limit of SimpleDB. Simply in SimpleDB, there is a link to the actual data stored in S3. ::

   S3 Wrapper

util.py
============

*Class* Crawler
-------------

**Workflow**

Crawler workers has the workflow as follows::
    
    while 1:
        t.reset()
        t.parse_sqs()
        if t.task is not None:
            t.generate_instance(*keys)
            t.retrieve()
            t.store()
        time.sleep(1)

Step 1. t.reset() to clear the stored data in the running instance.

Step 2. t.parse_sqs() just to retrieve the task from SQS service. If there is no new task (typically the task queue is empyt), then do nothing, else continue.

Step 3. t.generate_instance(*keys). With the keys, typical OAuth2 tokens from the OSN provider, it can generate an specific OSN wrapper instance to execute the API calls to OSN.

Step 4. t.retrieve(). Go and retrieve.

Step 5. t.store(). Store all the information in SimpleDB.

**Instructions**

The APIs have uniform interface of accessing multiple API resources. ::

    api.get_follers()
    api.get_friends()
    api.get_info()
    api.get_specifics()

All these APIs are derived from the online social network providers. These are the valid actions or instructions that the crawler is capable of calling its upper level api wrapper from OSN. ::
    
    self.crawler_resources = ['Twitter_Crawler', 'Facebook_Crawler', 'GooglePlus_Crawler', 'LinkedIn_Crawler']
    self.crawler_actions = ['get_followers', 'get_followings', 'get_friends', 'get_info', 'get_specifics']

**Storage Achitecture**

At first, for each OSN, we store the meta data, especially the "friends list" into SimpleDB or S3. For example, ::

    SimpleDB domain name: twitter-meta
    S3 bucket name: wisesoc.twitter.meta

One should notice that SimpleDB has 1024 byte limit for each value per item stored in it, so when the value exceed the limit length, we put an S3_LINK_INDICATOR (e.g. temperary as '###S3###') in SimpleDB and store the actual value in S3. 

*Class* Crawler Controller
-----------------------

Crawler controller controls what the crawlers should do. For example, running simple random walk and continuously issuing query tasks.::

    simple_random_walk()
    multithread_simple_random_walk()

multithread_simple_random_walk() runs multiple simple_random_walk to boost the sampling procedure. It simply run N simple random walks for roughly about one simple random walk's time.
