import sys
import time
import util
import thread


def load_keys_pool():
    keys_pool = []
    """The file should contains the key information one line per key, key is in list format.
    for example:
    ['yOA9DxpwUJSGvCaERmAZUA', 'gHAHDN5vrY5PaUdKK2Tju1beHiWnQaUBSpfzr1jI', '97692650-ma5aHV46wq5zQlcxmFE9hHNqugBbFe6rSqusBxxJm', 'QcXPzdBYQ8t6dOBL6gPIXsvnLgja9utbhOlITwBfk']
    """

    f = open('keys/twitter.txt', 'r')
    for line in f:
        line = eval(line)
        keys_pool.append(line)
    return keys_pool

def crawler_daemon(key, c_id):
    """docstring for crawler_daemon"""
    t = util.Crawler(c_id)

    #Typicla crawling cycle
    while 1:
        t.reset()
        t.parse_sqs()
        if t.task is not None:
            t.generate_instance(*key)
            t.retrieve()
            t.store()
        time.sleep(0.2)


def multithread_crawler_daemon(num_crawlers=10):

    keys_pool = load_keys_pool()

    if num_crawlers > len(keys_pool):
        num_crawlers = len(keys_pool)

    try:
        for i in range(num_crawlers):
            key = keys_pool[i]
            thread.start_new_thread(crawler_daemon, (key, 'c_' + str(i)))
    except:
       print "Error: unable to start thread"

    while 1:
        pass

if __name__ == "__main__":
    multithread_crawler_daemon()
