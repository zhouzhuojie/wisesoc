import boto
from boto.sqs.connection import SQSConnection
from boto.sqs.message import Message
from boto.s3.connection import S3Connection
from boto.s3.key import Key

##############################################
# Configuration of SQS services
access_key = ''
secret_key = ''
crawler_queue_name = 'crawler_queue'
sqs_conn = SQSConnection(access_key, secret_key)
crawler_queue = sqs_conn.create_queue(crawler_queue_name)

# Configuration of SimpleDB services
sdb_conn = boto.connect_sdb(aws_access_key_id= access_key,aws_secret_access_key=secret_key)
#sdb_meta = sdb_conn.create_domain('meta_sdb')
#sdb_rw = sdb_conn.create_domain('rw_sdb')

# Configuration of S3 interface
s3_conn= S3Connection(aws_access_key_id= access_key,aws_secret_access_key=secret_key)
#s3_meta = s3_conn.get_bucket('meta_s3')
#s3_rw= s3_conn.get_bucket('rw_s3')

def create_crawler_queue():
    """create a crawler queue, used for crawler controller"""
    sqs_conn.create_queue(crawler_queue_name)

def write_crawler_queue(message):
    """write message m to the crawler queue"""
    m = Message()
    m.set_body(message)
    status = crawler_queue.write(m)
    return status

def read_crawler_queue():
    """docstring for read_crawler_queue"""
    m = crawler_queue.read()
    if m is None:
        return None
    else:
        return m

def read_crawler_queue_all():
    """docstring for read_crawler_queue"""
    count_queue = crawler_queue.count()
    m_list = crawler_queue.get_messages(count_queue)
    for m in m_list:
        print m.get_body()

def delete_crawler_queue(m):
    """docstring for delete_craw"""
    crawler_queue.delete_message(m)

def clear_crawler_queue():
    """docstring for clear_crawler_queue"""
    crawler_queue.clear()

def create_simpledb(domain):
    return sdb_conn.create_domain(domain)

def get_simpledb(domain):
    return sdb_conn.get_domain(domain)

def write_simpledb(sdb, key, attr_dict):
    sdb.put_attributes(key, attr_dict)

def read_simpledb(sdb, key):
    return sdb.get_item(key)

def create_s3(bucket):
    return s3_conn.create_bucket(bucket)

def get_s3(bucket):
    return s3_conn.get_bucket(bucket)

def write_s3(s3, key, attr_dict):
    k = Key(s3)
    k.key = key
    k.set_contents_from_string(str(attr_dict))

def read_s3(s3, key):
    k = Key(s3)
    k.key = key
    return eval(k.get_contents_as_string())
