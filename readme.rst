Introduction
===================================

WiseSoc_ is a basic framework for building social network crawlers over the Amazon AWS cloud services. Generally, it integrated multiple online social network resources with their open APIs, such as Twitter, Facebook, Google+, etc. All their RESTful APIs are fully available through their OAuth interface. But almost all these APIs have rate limit which against data harvesters. WiseSoc leverages the power of Amazon AWS to build scalable sampling and crawling framework for online social networks.

.. _WiseSoc: https://bitbucket.org/zhouzhuojie/wisesoc/overview

Tutorial_

Features_

Tutorial
==================

Features
==================

Components
==================

aws.py
-------------

* SQS Services. SQS is used for creating crawling task queue.::

   create_crawler_queue()
   write_crawler_queue()
   read_crawler_queue()

* SimpleDB Services. SimpleDB serves as the cache and store two types tables.::

   SimpleDB Wrapper

* S3 Services. S3 storage serves as the cheap and flexible storage solution for those entries exceeds the limit of SimpleDB. Simply in SimpleDB, there is a link to the actual data stored in S3.::

   S3 Wrapper

util.py
-------------

**Class Crawler**

* The APIs have uniform interface of accessing multiple API resources.::

    api.get_follers()
    api.get_followings()
    api.get_info()
    api.get_specifics()

All these APIs are derived from the online social network providers.

* Tasks execution.::

    parse_sqs()
    retrive()
